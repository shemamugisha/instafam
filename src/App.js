import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import SignIn from './components/Authentication/SignIn';
import SignUp from './components/Authentication/SignUp';
import Overview from './components/Dashboard/Overview';
import Profile from './components/Dashboard/Profile';
import PrivateRoute from './components/Authentication/PrivateRoute';

function App() {
  return (
    <Router>
      <PrivateRoute exact path='/' component={Overview} />
      <PrivateRoute exact path='/profile' component={Profile} />
      <Route exact path='/signup' component={SignUp} />
      <Route exact path='/signin' component={SignIn} />
    </Router>
  );
}

export default App;
