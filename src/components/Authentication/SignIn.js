import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

function SignIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const history = useHistory();

  const userData = JSON.parse(localStorage.getItem('userData'));

  const data = userData.map((k) => {
    return { email: k.email, password: k.password };
  });

  const foundedEmail = data.map((user) => {
    return user.email;
  });

  const foundedPassword = data.map((user) => {
    return user.password;
  });

  const mail = foundedEmail.find((e) => email === e);
  const pass = foundedPassword.find((e) => password === e);

  const handleSubmit = async () => {
    const loginUser = [{ email: mail, password: pass }];
    if (!mail && !pass) {
      alert('User not found!');
    } else localStorage.setItem('user', JSON.stringify(loginUser));
    history.push('/');
    alert('success login');
  };

  return (
    <div className='auth--container'>
      <div className='auth--wrapper'>
        <h1 className='auth--title'>Welcome Back</h1>
        <form className='auth--form' onSubmit={handleSubmit}>
          <h2 className='auth--title form--title'>SignIn</h2>
          <div className='auth--card'>
            <label>Email</label>
            <input
              className='auth--input'
              type='email'
              required
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className='auth--card'>
            <label>Password</label>
            <input
              className='auth--input'
              type='password'
              required
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>

          <button className='auth--btn'>Sign In</button>
        </form>
      </div>
      <div style={{ textAlign: 'center', margin: '0.5rem' }}>
        Already have an account ? <Link to='/signup'>Sign Up</Link>
      </div>
    </div>
  );
}

export default SignIn;
