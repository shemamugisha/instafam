import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';

function SignUp() {
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const [userData, setUserData] = useState([
    { username: 'shema', email: 'shema@gmail.com', password: 'kn88chr4' },
    { username: 'issa', email: 'issa@gmail.com', password: 'kn88chr4' },
  ]);

  const history = useHistory();

  useEffect(() => {
    const data = localStorage.getItem('userData');

    if (data) {
      setUserData(JSON.parse(data));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('userData', JSON.stringify(userData));
    console.log('hhhhhhhhhhhh', userData);
  }, [userData]);

  const handleSubmit = (e) => {
    const newData = [
      ...userData,
      { email: email, username: username, password: password },
    ];
    e.preventDefault();
    setUserData(newData);
    localStorage.setItem('userData', JSON.stringify(newData));

    alert('Successfully Created Account');
    history.push('/signin');
  };

  return (
    <div className='auth--container'>
      <div className='auth--wrapper'>
        <h1 className='auth--title'>Instafam</h1>
        <form className='auth--form' onSubmit={handleSubmit}>
          <h2 className='auth--title form--title'>SignUp</h2>
          <div className='auth--card'>
            <label>Email</label>
            <input
              className='auth--input'
              type='email'
              value={email}
              required
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className='auth--card'>
            <label>Username</label>
            <input
              className='auth--input'
              type='text'
              required
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className='auth--card'>
            <label>Password</label>
            <input
              className='auth--input'
              type='password'
              required
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>

          <button className='auth--btn'>Sign Up</button>
        </form>
      </div>
      <div style={{ textAlign: 'center', margin: '0.5rem' }}>
        Already have an account ? <Link to='/signin'>Sign In</Link>
      </div>
    </div>
  );
}

export default SignUp;
