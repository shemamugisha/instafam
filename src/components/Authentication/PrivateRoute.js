import React, { useEffect, useState } from 'react';
import { Route, Redirect } from 'react-router-dom';

function PrivateRoute({ component: Component, ...rest }) {
  const [user, setUser] = useState();

  useEffect(() => {
    const userData = localStorage.getItem('user');
    console.log('kkkkkkkk', userData);
    setUser(JSON.parse(userData));
  }, []);

  console.log('uuserrrr', user);

  return (
    <Route
      {...rest}
      render={(props) => {
        if (user) {
          return <Component {...props} />;
        } else if (!user) {
          return <Redirect to='/signup' />;
        }
      }}
    />
  );
}

export default PrivateRoute;
