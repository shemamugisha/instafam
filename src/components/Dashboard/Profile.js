import React from 'react';
import Header from './Header';

function Profile() {
  const user = JSON.parse(localStorage.getItem('user'));

  const handleSubmit = () => {
    alert('profile updated');
  };

  return (
    <>
      <Header />
      <main className='profile--container'>
        <div className='auth--wrapper'>
          <h1 className='auth--title'>Update Profile</h1>
          {user.map((data) => (
            <form className='auth--form' onSubmit={handleSubmit}>
              <div className='auth--card'>
                <label>Email</label>
                <input
                  className='auth--input'
                  type='email'
                  value={data.email}
                />
              </div>
              <div className='auth--card'>
                <label>Password</label>
                <input
                  className='auth--input'
                  type='text'
                  value={data.password}
                />
              </div>
              <button className='auth--btn'>Update Profile</button>
            </form>
          ))}
        </div>
      </main>
    </>
  );
}

export default Profile;
