import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';

import { Link, useHistory } from 'react-router-dom';
function Header() {
  const [isOpen, setOpen] = useState(false);

  const history = useHistory();

  const logOut = () => {
    localStorage.removeItem('user');
    alert('good byee');
    history.push('/signin');
  };

  const toggleDropdown = () => setOpen(!isOpen);
  return (
    <header className='post--header'>
      <nav className='post--nav'>
        <div>
          <Link to='/' className='nav--title'>
            Instafam
          </Link>
        </div>
        <div>
          <button className='btns'>
            <FontAwesomeIcon
              className='icon dropdown-header'
              onClick={toggleDropdown}
              icon={faUser}
            />
          </button>
          <div className={`dropdown-body ${isOpen && 'open'}`}>
            <div className='dropdown-item'>
              <Link className='dropdown-link' to='/profile'>
                Profile
              </Link>
              <button
                onClick={() => logOut()}
                className='auth--btn dropdown-btn'
              >
                Logout
              </button>
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
}

export default Header;
