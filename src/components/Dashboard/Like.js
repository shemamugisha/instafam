import React, { useReducer } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';
const HANDLE_LIKE = Symbol('HANDLE_LIKE');
const HANDLE_DISLIKE = Symbol('HANDLE_DISLIKE');
const initialState = {
  likes: 0,
  active: null,
};

const reducer = (state, action) => {
  const { likes, active } = state;

  switch (action.type) {
    case HANDLE_LIKE:
      return {
        ...state,
        likes: state.likes + 1,
        active: 'like',
      };
    case HANDLE_DISLIKE:
      return {
        ...state,
        likes: active === 'like' ? likes - 1 : likes,
        active: 'dislike',
      };
    default:
      return state;
  }
};

const Like = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { likes, active } = state;
  return (
    <div className='post--like'>
      <button
        className='btns'
        style={{
          color: active === 'like' ? 'green' : 'black',
        }}
        onClick={() =>
          active !== 'like' ? dispatch({ type: HANDLE_LIKE }) : null
        }
      >
        <FontAwesomeIcon className='icon' icon={faThumbsUp} />
      </button>
      <p style={{ padding: '0 .5rem' }}>
        Likes: <span>{likes}</span>
      </p>
    </div>
  );
};

export default Like;
