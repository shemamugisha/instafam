import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import Header from './Header';
import { postDatas } from '../../util/Datas';
import Like from './Like';

function Overview() {
  const [createPost, setCreatePost] = useState('');
  const [posts, setPosts] = useState([
    { createPost: 'Lorem ipsum dolor sit amet consectetur adipisicing elit' },
  ]);

  const allposts = [...postDatas, posts];

  useEffect(() => {
    const data = localStorage.getItem('posts');

    if (data) {
      setPosts(JSON.parse(data));
    }
  }, []);

  useEffect(() => {
    allposts.find((posts) =>
      localStorage.setItem('posts', JSON.stringify(posts)),
    );
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    setPosts([...posts, { createPost: createPost }]);

    alert('Successfully Created post');
  };

  return (
    <>
      <Header />
      <main className='post--container'>
        <section className='post--wrapper'>
          {posts.map((posts) => (
            <div className='post--card'>
              <div className='post--content'>
                <p className='content'>{posts.createPost}</p>
                <div className='post--btns'>
                  <Like />
                  <div>
                    <button className='btns edit-btn'>
                      <FontAwesomeIcon
                        className='icon edit-icon'
                        icon={faEdit}
                      />
                    </button>
                    <button className='btns delete-btn'>
                      <FontAwesomeIcon
                        className='icon delete-icon'
                        icon={faTrashAlt}
                      />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </section>
        <section className='create--post'>
          <form className='post--form' onSubmit={handleSubmit}>
            <div className='auth--card'>
              <label>Text: </label>
              <textarea
                cols='30'
                rows='10'
                value={createPost}
                required
                onChange={(e) => setCreatePost(e.target.value)}
              />
            </div>
            <button className='auth--btn'>Create Post</button>
          </form>
        </section>
      </main>
    </>
  );
}

export default Overview;
